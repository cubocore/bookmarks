project( corepins )
cmake_minimum_required( VERSION 3.16 )

set( PROJECT_VERSION 5.0.0 )
set( PROJECT_VERSION_MAJOR 5 )
set( PROJECT_VERSION_MINOR 0 )
set( PROJECT_VERSION_PATCH 0 )

set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )
add_compile_definitions(VERSION_TEXT="${PROJECT_VERSION}")

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )
set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTORCC ON )
set( CMAKE_AUTOUIC ON )

add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

find_package( Qt6Core REQUIRED )
find_package( Qt6Gui REQUIRED )
find_package( Qt6Widgets REQUIRED )

set( SOURCES
    src/main.cpp
    src/corepins.cpp
    src/corepins.h
    src/settings.cpp
    src/settings.h
)

set( UIS
    src/corepins.ui
)

add_executable( corepins ${SOURCES} ${UIS} )
target_link_libraries ( corepins  Qt6::Core Qt6::Gui Qt6::Widgets cprime-widgets cprime-core )

install( TARGETS corepins DESTINATION bin )
install( FILES cc.cubocore.CorePins.desktop DESTINATION share/applications )
install( FILES cc.cubocore.CorePins.svg DESTINATION share/icons/hicolor/scalable/apps/ )
